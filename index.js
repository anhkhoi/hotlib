const _ = require('lodash');
const execa = require('execa');

// Common Packages used through on projects
const rootPackages = require('./package.json');

// Package for liba
const libaPackages = require('./liba/package.json');

// Package for libb
const libbPackages = require('./libb/package.json');

const totalPackages = _.merge(
  rootPackages.dependencies,
  libaPackages.dependencies,
  libbPackages.dependencies
);

(async () => {
  const packageStrings = _.join(_.map(totalPackages, function(value, key) {
    return `${key}@${value}`;
  }), ' ');
  execa.command(`npm install ${packageStrings}`);
})();