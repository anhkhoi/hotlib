import { parsePhoneNumberFromString } from 'libphonenumber-js';

export default class LibB {
  run() {
    const phoneNumber = parsePhoneNumberFromString('Phone: 8 (800) 555 35 35.', 'RU');
    console.log('running - libb', phoneNumber);
  };
};
